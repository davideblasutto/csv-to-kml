exports.convert = function(ctkOptions) {

	//
	// ## Log (if requested by user)
	//
	let log = function(message) {

		if (ctkOptions.verbose)
			console.log(message)

	}

	// Default options
	ctkOptions.csvOptions = ctkOptions.csvOptions || { delimiter : ';' }
	ctkOptions.geocoderOptions = ctkOptions.geocoderOptions || { provider: 'google' }
	ctkOptions.documentName = ctkOptions.documentName || 'My KML Document'
	ctkOptions.documentDescription = ctkOptions.documentDescription || ''
	ctkOptions.verbose = ctkOptions.verbose || false

	// Convert CSV data to Object with options
	let addresses = require('csvjson').toObject(
		ctkOptions.data,
		ctkOptions.csvOptions
	)

	// Intialize geocoder with options
	let geocoder = require('node-geocoder')(ctkOptions.geocoderOptions)

	// Keep all promises in an array to know whent they are all completed
	var promises = []

	log('Finding coordinates for ' + addresses.length +  ' addresses...')

	// Start a promise, which is the result of convert()
	return new Promise(function (fulfill, reject) {

		// For each address
		addresses.forEach(address => {

			// Compose full address string
			let addressString =
					address.Address1 + ' ' +
					address.Address2 + ', ' +
					address['ZIP/PostalCode'] + ' ' +
					address.City + ' ' +
					address['State/Province/Region'] + ', ' +
					address.Country

			// Send request
			let promise = geocoder.geocode(addressString)

			promise.then(result => {

				if (result.length == 0) {

					// No results
					address.found = false

					log(' - ' + address.Name + ' → No results')

				} else {

					// Coordinates obtained for address
					address.found = true

					log(' - ' + address.Name + ' → ' + result[0].latitude + ', ' + result[0].longitude)

					// Add coordinates to address object
					address.latitude = result[0].latitude
					address.longitude = result[0].longitude

				}

			}).catch(error => reject(error))

			promises.push(promise)

		})

		// Wait for all promises to be completed
		Promise.all(promises).then(value => {

			log('Found coordinates for ' + addresses.filter(a => a.found).length + ' addresses out of ' + addresses.length)

			// Convert addresses array to GeoJSON
			var geoJSON = {
				type: 'FeatureCollection',
				features: []
			}

			// For each address
			addresses.forEach(address => {

				geoJSON.features.push({
					type: 'Feature',
					properties: {
						name: address.Name,
						description: address.Description
					},
					geometry: {
						type: 'Point',
						coordinates: [ address.longitude, address.latitude ]
					}
				})

			})

			// Convert GeoJSON to KML
			let kmlData = require('tokml')(
				geoJSON,
				{
					name: 'name',
					description: 'description',
					documentName: ctkOptions.documentName,
					documentDescription: ctkOptions.documentDescription
				}
			)

			log('Generated KML (' + kmlData.length + ' chars)')

			// The main promise is fulfilled
			fulfill(kmlData)

		})

	})

}
