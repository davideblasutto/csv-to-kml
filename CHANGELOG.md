# 1.0.2 (05/01/2019)

* Minor changes

# 1.0.1 (25/10/2017)

* Added README
* Better test

# 1.0.0 (24/10/2017)

First release
