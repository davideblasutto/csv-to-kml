# csv-to-kml

Geocodes addresses from CSV and generates a KML file.

## Usage

### Example
```javascript
require("csv-to-kml").convert({
	data: csvData,
	verbose: true,
	geocoderOptions: { provider: "google" },
	csvOptions: { delimiter : "|" },
	documentName: "A List of Places"
}).then(kmlData => {
	// Do whatever you want with the KML generated
}).catch(error => {
	// Catch errors
})
```

### Options
 * `csvData` - **Required**. CSV data read from a file with `fs` or produced in any other way.
 * `verbose` - *Optional*. If `true` will log some stuff on console. Default: `false`.
 * `csvOptions` - *Optional*. This object will be passed to the CSV parser. See (csvjson)[https://www.npmjs.com/package/csvjson] for details. Default `{ delimiter : ";" }`.
 * `geocoderOptions` - *Optional*. This object will be passed to the geocoder. See (node-geocoder)[https://www.npmjs.com/package/node-geocoder] for details. Default `{ provider: "google" }`.
 * `documentName` - *Optional*. Value used for the `name` tag of the KML document. Default: `My KML Document`.
 * `documentDescription` - *Optional*. Value used for the `description` tag of the KML document. Default: ``.
