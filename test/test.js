let fs = require('fs')

// Delete output file if already exists
if (fs.existsSync('./test/output.kml'))
	fs.unlinkSync('./test/output.kml')

// Read data from CSV
let csvData = fs.readFileSync(
	'./test/input.csv',
	{ encoding : 'utf8' }
)

// Test configuration
let testConfig = require('../test/test-config.json')

// Use main module
require('../index.js').convert({
	data: csvData,
	verbose: true,
	geocoderOptions: {
		provider: 'google',
		apiKey: testConfig.googleApiKey
	},
	documentName: '30 - Zero Branco'
}).then(kmlData => {
	// Save KML
	fs.writeFileSync(
		'./test/30.kml',
		kmlData,
		{ encoding : 'utf8' }
	)
}).catch(error => {
	// Error!
	console.log('ERROR:', error)
})
